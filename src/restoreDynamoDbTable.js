/**
 * This will change Integer hask keys to strings.
 */
const AWS = require('aws-sdk');
const fs = require('fs');
const config = require('config.json')('../config.json');

AWS.config.update({
  accessKeyId: config.accessKeyId,
  secretAccessKey: config.secretAccessKey,
  region: config.region
});

var docClient = new AWS.DynamoDB.DocumentClient();

const tableName = config.tableName;
const fileName = '../dump/'+tableName+'.json';

fs.readFile(fileName, 'utf-8',function (err, data) {
  if (err) throw err;
  var items = JSON.parse(data).Items;
  items.forEach(function(item) {
    var params = {
      TableName: tableName,
      Item: {
        partitionId: item.partitionId+"",
        sortId: item.sortId,
        dummyData: item.dummyData

      }
    }

    docClient.put(params, function(error, data){
      if(error) console.log(error);
      console.log(data);
    });
  }, this);

});
