/**
 * This will dump dynamodb table
 */
const AWS = require('aws-sdk');
const fs = require('fs');
const config = require('config.json')('../config.json');

AWS.config.update({
  accessKeyId: config.accessKeyId,
  secretAccessKey: config.secretAccessKey,
  region: config.region
});

var docClient = new AWS.DynamoDB.DocumentClient();

const tableName = config.tableName;
const fileName = '../dump/'+tableName+'.json';

var params = {
  TableName: tableName,
};

var queryExecute = function () {
  docClient.scan(params, function (err, result) {
    if (err) {
      console.log(err);
    } else {
      console.log("Writing into "+fileName+" ...");
      fs.writeFile(fileName, JSON.stringify(result, null, 2), 'utf-8',function (err) {
        if (err) return console.log(err);
        console.log('Finished...');
      });
      if (result.LastEvaluatedKey) {
        params.ExclusiveStartKey = result.LastEvaluatedKey;
        queryExecute();
      }
    }
  });
}
queryExecute();
